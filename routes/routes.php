<?php

use \Illuminate\Support\Facades\Route;
use \Laramods\ConfigSettings\Controllers\ConfigSettingsController;



$middleware = config('laramods.config-settings.route.middleware', ['web', 'auth']);
$prefix = config('laramods.config-settings.route.prefix', '/');

Route::middleware($middleware)
    ->prefix($prefix)->group(function(){
        Route::resource('settings', ConfigSettingsController::class)
            ->only(['index', 'store']);
    });
