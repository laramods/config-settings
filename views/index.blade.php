@extends('layouts.app')

@section('content')

    <div class="container">

        {{ Form::open([
            'route' => 'settings.index',
            'autocomplete' => 'off',
            'class' => 'form-settings'
        ]) }}


        <div class="row">

            <div class="col-sm-3">
                <div class="nav flex-column nav-pills mb-3">
                    @php($active = 'active')
                    @foreach($pages as $page_key => $page)
                        <a href="#settings-tab-{{ $page_key }}"
                           data-toggle="pill"
                           class="nav-link {{ $active }}">{{ $page['title'] }}</a>
                        @php($active = '')
                    @endforeach
                </div>
            </div>

            <div class="col-sm-9">
                <div class="tab-content">

                    @php($active = 'show active')
                    @foreach($pages as $page_key => $page)

                        <div class="tab-pane fade {{ $active }}" id="settings-tab-{{ $page_key }}">
                            <div class="settings accordion" id="settings-page-{{ $page_key }}">

                                @php($groups = $page['_groups'] ?: [])

                                @foreach( $groups as $group_key => $group )

                                    <div class="card">
                                        <div class="card-header">
                                            <button class="btn btn-link btn-block text-dark text-left collapsed text-decoration-none"
                                                    type="button"
                                                    data-toggle="collapse"
                                                    data-target="#settings-body-{{ $group_key }}">
                                                <strong>{{ $group['title'] ?? $group_key }}</strong>
                                                @if( $group['description'] ?? NULL )
                                                    <small class="d-block text-muted">{{ $group['description'] }}</small>
                                                @endif
                                            </button>
                                        </div>

                                        <div id="settings-body-{{ $group_key }}"
                                             class="collapse"
                                             data-parent="#settings-page-{{ $page_key }}">

                                            <div class="card-body">

                                                @php($fields = $group['_fields'] ?: [])

                                                @foreach($fields as $field_key => $field)

                                                    <div class="form-group row">

                                                        {{ Form::label($field_key, $field['title'], [
                                                            'class' => 'col-form-label col-sm-3'
                                                        ]) }}

                                                        <div class="col">
                                                            {{ Form::{$field['type'] ?? 'text'}($field_key, config($field_key), [
                                                                'name' => \Laramods\ConfigSettings\Facades\ConfigSettingsFacade::dottedToInputArray($field_key),
                                                                'class' => 'form-control',
                                                                'placeholder' => $field['placeholder'] ?? $field['title'] ?? $field_key,
                                                            ]) }}
                                                            @if($field['description'] ?? false)
                                                                <small class="text-muted">{{ $field['description'] }}</small>
                                                            @endif
                                                        </div>

                                                    </div>

                                                @endforeach

                                            </div>
                                        </div>
                                    </div>

                                @endforeach

                            </div>
                        </div>

                        @php($active = '')
                    @endforeach

                </div>

                <div class="my-3">
                    {{ Form::submit('Save Changes', ['class' => 'btn btn-primary btn-settings']) }}
                </div>

            </div>

        </div>

        {{ Form::close() }}


    </div>

@endsection
