<?php

namespace Laramods\ConfigSettings\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laramods\ConfigSettings\Facades\ConfigSettingsFacade as CSFacade;


class ConfigSettingsController extends Controller
{

    public function index(){

        $pages = CSFacade::fieldsInPages();

        return view('laramods.config-settings::index',
            compact('pages')
        );

    }


    public function store(Request $request){

        $fields = CSFacade::fields();
        $keys = array_keys($fields);

        $data = $request->only($keys);

        foreach ( $data as $key => $settings ){
            CSFacade::set($key, $settings);
        }


        return back();

    }


}
