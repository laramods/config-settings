<?php

namespace Laramods\ConfigSettings;


use Illuminate\Support\Arr;
use Laramods\ConfigSettings\Models\ConfigSetting;

class ConfigSettings
{


    public function loadSettings(){
        $settings = ConfigSetting::query()->pluck('value', 'config')->toArray();
        foreach ( $settings as $key => $setting ){
            $this->mergeSetting($key, $setting);
        }
    }


    /**
     * @param string $key
     * @param array $value
     */
    public function mergeSetting($key, $value){
        $app = app();
        $app['config']->set($key, array_replace_recursive(
            $app['config']->get($key, []), $value
        ));
    }


    /**
     * @param string $key
     * @param array $value
     * @return array|mixed
     */
    public function set(string $key, array $value){

        $keys = explode('.', $key);
        $base_key = Arr::first($keys);

        $settings = $this->get($base_key, []);
        Arr::set($settings, $key, $value);
        $storable_settings = array_pop($settings);

        ConfigSetting::query()->updateOrCreate(
            ['config' => $base_key,],
            ['value' => $storable_settings,]
        );

        $this->mergeSetting($base_key, $storable_settings);

        return $settings;
    }


    /**
     * @param $key
     * @param $default
     * @return array|mixed
     */
    public function get($key, $default = []){

        $settings = ConfigSetting::config($key)
            ->pluck('value', 'config')
            ->toArray();

        if( empty($settings) ){
            return $default;
        }

        return $settings;
    }


    /**
     * Get registered fields.
     *
     * @param array $default
     * @return \Illuminate\Config\Repository|mixed
     */
    public function fields($default = []){
        return config('laramods.config-settings.fields') ?? $default;
    }


    /**
     * Get available field groups.
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    public function groups(){
        return config('laramods.config-settings.groups');
    }



    /**
     * Get available settings pages.
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    public function pages(){
        return config('laramods.config-settings.pages', [
            'primary' => [
                'title' => 'Primary Settings',
            ],
        ]);
    }


    public function fieldsInPages(){

        $pages = $this->pages();
        $groups = $this->groups();
        $fields = $this->fields();

        foreach ( $fields as $field_key => $field ){
            if( $groups[$field['group']] ?? false ){
                $groups[$field['group']]['_fields'][$field_key] = $field;
            }
        }

        foreach ($groups as $group_key => $group){

            if(!isset($group['page'])){
                $groups[$group_key]['page'] =
                    config('laramods.config-settings.default_page', 'primary');
            }

            if( $pages[$group['page']] ?? false ){
                $pages[$group['page']]['_groups'][$group_key] = $group;
            }
        }

        return $pages;

    }


    /**
     * @param string $dotted
     * @return string
     */
    public function dottedToInputArray(string $dotted): string {
        $pieces = explode('.', $dotted);
        if( count( $pieces ) === 1 ){
            return $dotted;
        }


        $string = '';
        foreach ($pieces as $index => $piece){

            if( $index > 0 ){
                $string .= '[';
            }

            $string .= $piece;

            if( $index > 0 ){
                $string .= ']';
            }
        }

        return $string;

    }

}
