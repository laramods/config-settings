<?php

namespace Laramods\ConfigSettings\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $config
 * @property array $value
 */
class ConfigSetting extends Model
{

    public $timestamps = false;


    protected $fillable = [
        'config', 'value'
    ];

    protected $casts = [
        'value' => 'array',
    ];

    public function scopeConfig(Builder $query, string $config){
        $query->where('config', $config);
    }

}
