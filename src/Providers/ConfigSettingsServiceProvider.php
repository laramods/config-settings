<?php

namespace Laramods\ConfigSettings\Providers;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Laramods\ConfigSettings\Facades\ConfigSettingsFacade as CSFacade;
use Laramods\ConfigSettings\Models\ConfigSetting;
use Laramods\ConfigSettings\ConfigSettings;

class ConfigSettingsServiceProvider extends ServiceProvider
{



    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){


        $this->loadRoutesFrom(__DIR__.'/../../routes/routes.php');

        $this->loadMigrationsFrom(__DIR__.'/../../migrations');

        $this->publishes([
            __DIR__.'/../../migrations' => database_path('migrations'),
        ], ['laramods', 'laramods_config_settings', 'laramods_config_settings_migrations', 'migrations']);


        $this->publishes([
            __DIR__ . '/../../config/config-settings.php' => config_path('laramods/config-settings.php'),
        ], ['laramods', 'laramods_config_settings', 'laramods_config_settings_config', 'config']);


        $this->loadViewsFrom(__DIR__.'/../../views', 'laramods.config-settings');

        $this->publishes([
            __DIR__.'/../../views' => resource_path('views/vendor/laramods.config-settings'),
        ], ['laramods', 'laramods_config_settings', 'laramods_config_settings_views', 'views']);


        $this->mergeConfigWithSettings();

    }


    /**
     * Register the application services.
     *
     * @return void
     * @throws \Throwable
     */
    public function register()
    {

        $this->app->bind('laramods.config-settings', ConfigSettings::class);


        $this->mergeConfigFrom(
            __DIR__ . '/../../config/config-settings.php', 'laramods.config-settings'
        );

    }



    public function mergeConfigWithSettings(){

        try{

            DB::connection()->getPdo();

            $table = with(new ConfigSetting())->getTable();
            if( !Schema::hasTable( $table ) ) return;

            CSFacade::loadSettings();

        }catch (\Exception $e){

        }

    }


}
