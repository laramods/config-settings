<?php

namespace Laramods\ConfigSettings\Facades;


use Illuminate\Support\Facades\Facade;
use Laramods\ConfigSettings\ConfigSettings;


/**
 * @see ConfigSettings
 *
 * Class ConfigSettingsFacade
 * @package Laramods\ConfigSettings\Facades
 *
 * @method static mergeSetting(string $key, mixed $value) Merges settings.
 * @method static loadSettings() Loads & merges settings from database to original config.
 * @method static array fields(mixed $default = []) Get registered fields.
 * @method static array groups() Get registered field groups.
 * @method static string dottedToInputArray(string $dotted) Convert dot notation into input field array notation.
 * @method static string set(string $key, array $value) Save config into settings.
 * @method static array fieldsInPages() Get complete settings fields hierarchy (pages/groups/fields).
 */
class ConfigSettingsFacade extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'laramods.config-settings';
    }

}
